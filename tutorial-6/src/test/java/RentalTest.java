import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {
    Movie movie;
    Rental rent;

    @Before
    public void setUp() {
        this.movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        this.rent = new Rental(movie, 3);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void getThisAmountNRtest() {
        this.movie.setPriceCode(1);
        assertEquals(9, rent.getThisAmount(), 0.000000001);
    }

    @Test
    public void getThisAmountCtest() {
        this.movie.setPriceCode(2);
        assertEquals(1.5, rent.getThisAmount(), 0.000000001);
    }

    @Test
    public void getThisAmountCMoretest() {
        Movie child = new Movie(("Baby Days Out"), Movie.CHILDREN);
        Rental newRent = new Rental(child, 5);
        assertEquals(4.5, newRent.getThisAmount(), 0.000000001);
    }


}
