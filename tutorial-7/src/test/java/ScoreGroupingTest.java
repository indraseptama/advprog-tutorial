import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;



public class ScoreGroupingTest {

    private Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        scores.put("Indras", 9);
        scores.put("Nabil", 11);
        scores.put("Fira", 7);
        scores.put("Denis", 11);
        scores.put("Toei", 11);
        scores.put("David", 7);
    }

    @Test
    public void groupByScores() {
        assertEquals("{7=[Fira, David], 9=[Indras], 11=[Toei, Nabil, Denis]}",
                ScoreGrouping.groupByScores(scores).toString());
    }
}
